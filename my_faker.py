import random
from datetime import timedelta

from faker import Faker
from faker.providers import internet, date_time

AMOUNT_OF_SOCKET_TYPES = 3
AMOUNT_OF_PAYMENT_SERVICES = 5
AMOUNT_OF_PAYMENTS_PER_USER = 5
AMOUNT_OF_CARS = 50
AMOUNT_OF_ORDERS = 100
AMOUNT_OF_ADDRESSES = 500
AMOUNT_OF_DISTRICTS = 6
AMOUNT_OF_CHARGING_STATIONS = 20
AMOUNT_OF_WORKSHOPS = 3
AMOUNT_OF_CAR_TYPES = 3
AMOUNT_OF_CAR_PART_TYPES = 10
AMOUNT_OF_CAR_PARTS = 30
AMOUNT_OF_CAR_PART_REQUESTS = 30

socket_sizes = ['XS', 'S', 'M', 'L', 'XL']
statuses = ['created', 'canceled', 'in_progress', 'ended']
countries = ['Russia', 'The USA', 'Canada']
colors = ['red', 'blue', 'white', 'super-white']


def fake_db(db):
    faker = Faker()
    faker.add_provider(internet)

    payment_services_ids = []
    payments_ids = []
    user_payments_ids = []
    customer_ids = []
    socket_type_ids = []
    car_ids = []
    address_ids = []
    order_ids = []
    district_ids = []
    city_ids = []
    country_ids = []
    charging_station_ids = []
    workshop_ids = []
    car_part_ids = []
    car_type_ids = []
    part_ids = []
    for country_name in countries:
        sql = "INSERT INTO country (name) values (%s) RETURNING id;"
        data = (country_name,)
        cur = db.run_sql(sql, data)

        country_id = cur.fetchone()[0]
        country_ids.append(country_id)

    # let's create Kazan
    country_name = 'Russia'
    sql = "SELECT id from country where name=%s"
    data = (country_name,)
    cur = db.run_sql(sql, data)
    russia_id = cur.fetchone()[0]

    sql = "INSERT INTO city (name, country_id) values (%s, %s) RETURNING id;"
    data = ('Kazan', russia_id)
    cur = db.run_sql(sql, data)
    kazan_id = cur.fetchone()[0]

    for i in range(AMOUNT_OF_PAYMENT_SERVICES):
        name = faker.domain_word()
        add_customer_sql = "INSERT INTO payment_service (name) values (%s) RETURNING id;"
        data = (name,)
        cur = db.run_sql(add_customer_sql, data)
        payment_id = cur.fetchone()[0]
        payment_services_ids.append(payment_id)

        print(f'Inserted payment service with id {id} ')

    for i in range(10):
        first_name = faker.name().split(' ')[0]
        second_name = faker.name().split(' ')[1]
        last_name = faker.name().split(' ')[1]
        username = faker.user_name()
        email = faker.free_email()
        is_verified = True
        phone_number = random.randint(9000000000, 9999999999)

        sql = "INSERT INTO customer (first_name, second_name, last_name, username, email, is_verified, phone_number) values (%s, %s, %s, %s, %s, %s, %s ) RETURNING id;"
        data = (first_name, second_name, last_name, username, email, is_verified, phone_number)
        cur = db.run_sql(sql, data)

        customer_id = cur.fetchone()[0]

        customer_ids.append(customer_id)

        print(f'Inserted customer with id {customer_id} ')

        for j in range(AMOUNT_OF_PAYMENTS_PER_USER):
            amount = random.randint(1, 1000)
            payment_service_id = random.choice(payment_services_ids)
            date = faker.date_time_this_year(after_now=False)

            sql = "INSERT INTO payment (amount, payment_service_id, customer_id, date) values (%s, %s, %s, %s) RETURNING id;"
            data = (amount, payment_service_id, customer_id, date)
            cur = db.run_sql(sql, data)
            user_payment_id = cur.fetchone()[0]
            user_payments_ids.append(user_payment_id)

            print(f'Inserted payment with id {user_payment_id} ')

    for i in range(AMOUNT_OF_SOCKET_TYPES):
        shape = faker.domain_word()
        size = random.choice(socket_sizes)

        sql = "INSERT INTO socket_type (shape, size) values (%s, %s) RETURNING id;"
        data = (shape, size)
        cur = db.run_sql(sql, data)

        socket_id = cur.fetchone()[0]
        socket_type_ids.append(socket_id)

        print(f'Inserted socket_type with id {socket_id} ')

    for i in range(AMOUNT_OF_CARS):
        socket_type = random.choice(socket_type_ids)
        vin = f'{random.randint(0,9)}{faker.text().upper()[:4]}{random.randint(10, 20)}{faker.text().upper()[:2]}{random.randint(100000, 999999)}'
        is_online = random.choice([True, False])
        color = random.choice(colors)
        plate = faker.text()[:10]

        sql = "INSERT INTO car (socket_type_id, vin, is_online, color, plate) values (%s, %s, %s, %s, %s) RETURNING id;"
        data = (socket_type, vin, is_online, color, plate)
        cur = db.run_sql(sql, data)

        car_id = cur.fetchone()[0]
        car_ids.append(car_id)

        print(f'Inserted socket_type with id {car_id} ')

    for i in range(AMOUNT_OF_DISTRICTS):
        zip_code = random.randint(420000, 420500)
        city_id = kazan_id

        sql = "INSERT INTO district (zip_code, city_id) values (%s, %s) RETURNING id;"
        data = (zip_code, city_id)
        cur = db.run_sql(sql, data)

        district_id = cur.fetchone()[0]
        district_ids.append(district_id)

    for i in range(AMOUNT_OF_ADDRESSES):
        address = faker.address()
        line_1, line_2 = address.split('\n')
        point = str(
            (
                random.uniform(48.5660806, 49.5660806),
                random.uniform(55.3304307, 56.3304307)))  # Coordinates around Kazan
        district_id = random.choice(district_ids)

        sql = "INSERT INTO address(address_line_1, address_line_2, point, disctrict_id) values (%s, %s, %s, %s) RETURNING id;"
        data = (line_1, line_2, point, district_id)
        cur = db.run_sql(sql, data)

        address_id = cur.fetchone()[0]
        address_ids.append(address_id)

    for i in range(AMOUNT_OF_ORDERS):
        started_at = faker.date_time_this_year(after_now=False)
        ended_at = faker.date_time_between_dates(datetime_start=started_at)
        starting_address = random.choice(address_ids)
        ending_address = random.choice(address_ids)
        status = 'ended'
        car_id = random.choice(car_ids)
        customer_id = random.choice(customer_ids)
        car_initial_point = str(
            (
                random.uniform(48.5660806, 49.5660806),
                random.uniform(55.3304307, 56.3304307))
        )  # Coordinates around Kazan

        sql = "INSERT INTO \"order\" (started_at, ended_at, starting_address, ending_address, status, car_id, customer_id, car_initial_point) values (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING id;"
        data = (started_at, ended_at, starting_address, ending_address, status, car_id, customer_id, car_initial_point)
        cur = db.run_sql(sql, data)

        order_id = cur.fetchone()[0]
        order_ids.append(order_id)

        print(f'Inserted order with id {order_id} ')

    for i in range(AMOUNT_OF_CHARGING_STATIONS):
        price = random.randint(1, 20)
        max_time = random.randint(2, 7)
        address_id = random.choice(address_ids)

        sql = "INSERT INTO \"charging_station\" (price_per_hour, max_time_for_charging, address_id) values (%s, %s, %s) RETURNING id;"
        data = (price, max_time, address_id)
        cur = db.run_sql(sql, data)

        charging_station_id = cur.fetchone()[0]
        charging_station_ids.append(charging_station_id)

        print(f'Inserted charging station with id {order_id} ')

    for i in range(AMOUNT_OF_CHARGING_STATIONS * AMOUNT_OF_CARS):
        charging_station_id = random.choice(charging_station_ids)
        car_id = random.choice(car_ids)
        started_at = faker.date_time_this_year(after_now=False) - timedelta(hours=5)  # for preventing dates in future
        ended_at = started_at + timedelta(hours=random.randint(1, 2))

        # let's add this socket_type to compatible
        sql = "SELECT socket_type_id from car where id=%s"
        data = (car_id,)
        cur = db.run_sql(sql, data)
        car_socket_type_id = cur.fetchone()[0]

        sql = "INSERT INTO \"charging_station_socket_types\" (charging_station_id, socket_type_id, amount ) values (%s, %s, %s);"
        data = (charging_station_id, car_socket_type_id, random.randint(10, 20))
        cur = db.run_sql(sql, data)

        # insert record about charging
        sql = "INSERT INTO \"charging_station_car\" (charging_station_id, car_id, started_at,ended_at ) values (%s, %s, %s, %s);"
        data = (charging_station_id, car_id, started_at, ended_at)
        cur = db.run_sql(sql, data)

    for i in range(AMOUNT_OF_WORKSHOPS):
        weekday_working_hours_starts = '8:00:00'
        weekday_working_hours_ends = '18:00:00'

        holiday_working_hours_starts = '8:00:00'
        holiday_working_hours_ends = '18:00:00'
        located_at = random.choice(address_ids)

        sql = "INSERT INTO workshop (weekday_working_hours_starts, weekday_working_hours_ends, holiday_working_hours_starts, holiday_working_hours_ends, located_at) VALUES (%s, %s, %s, %s, %s) RETURNING id;"

        data = (weekday_working_hours_starts, weekday_working_hours_ends, holiday_working_hours_starts,
                holiday_working_hours_ends, located_at)
        cur = db.run_sql(sql, data)

        workshop_id = cur.fetchone()[0]
        workshop_ids.append(workshop_id)

        print(f'Inserted workshop_id with id {workshop_id} ')

    for i in range(AMOUNT_OF_CAR_PART_TYPES):
        name = faker.name()
        price = random.randint(1, 100)

        sql = "INSERT INTO car_part (name, price) VALUES (%s, %s)  RETURNING id;"
        data = (name, price)

        cur = db.run_sql(sql, data)

        car_part_id = cur.fetchone()[0]
        car_part_ids.append(car_part_id)

        print(f'Inserted car_part_id with id {car_part_id} ')

    for i in range(AMOUNT_OF_CAR_PARTS):
        car_id = random.choice(car_ids)
        workshop_id = random.choice(workshop_ids)
        started_at = faker.date_time_this_year(after_now=False) - timedelta(hours=5)  # for preventing dates in future
        ended_at = started_at + timedelta(hours=random.randint(1, 2))

        sql = "INSERT INTO workshop_car (car_id, workshop_id, started_at, ended_at) VALUES (%s, %s, %s, %s) RETURNING id;"
        data = (car_id, workshop_id, started_at, ended_at)

        cur = db.run_sql(sql, data)

        part_id = cur.fetchone()[0]
        part_ids.append(part_id)

        print(f'Inserted workshop_car with id {part_id} ')

    for i in range(AMOUNT_OF_CAR_PARTS):
        car_part_id = random.choice(car_part_ids)
        workshop_id = random.choice(workshop_ids)

        sql = "INSERT INTO workshop_car_car_part (car_part_id, workshop_car_id) VALUES (%s, %s) RETURNING id;"
        data = (car_part_id, workshop_id)

        cur = db.run_sql(sql, data)

        part_id = cur.fetchone()[0]
        part_ids.append(part_id)

        print(f'Inserted part_id with id {part_id} ')
