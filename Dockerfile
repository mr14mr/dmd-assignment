FROM python:3.7-alpine3.8

WORKDIR /app

RUN apk --no-cache add build-base
RUN apk --no-cache add postgresql-dev

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD python app.py