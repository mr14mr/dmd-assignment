import json
import logging

from flask import Flask
from flask import render_template, request

from queries import QueryManager

app = Flask(__name__)

query_manager = QueryManager()


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/1/', methods=['GET', 'POST'])
def first():
    if request.method == 'GET':
        return render_template('first.html')

    cars = query_manager.query_1(started_at=request.form['started_at'], ended_at=request.form['ended_at'],
                                 color=request.form['color'], plate=request.form['plate'],
                                 customer_id=request.form['customer_id'])
    data = []

    for car in cars:
        el = {
            'id': car[0],
            'vin': car[1],
            'color': car[4],
            'plate_number': car[5]
        }
        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/2/', methods=['GET', 'POST'])
def second():
    if request.method == 'GET':
        return render_template('second.html')

    items = query_manager.query_2(date=request.form['date'])
    data = []

    for item in items:
        el = {
            'time': item[0].strftime('%H') + '-' + item[1].strftime('%H'),
            'number': item[2]
        }
        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/3/', methods=['GET', 'POST'])
def third():
    if request.method == 'GET':
        return render_template('first.html')

    items = query_manager.query_3(start_date=request.form['start_date'])
    data = []

    for item in items:
        el = {
            'morning': item[0],
            'afternoon': item[1],
            'evening': item[2],
        }
        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/4/', methods=['GET', 'POST'])
def fourth():
    if request.method == 'GET':
        return render_template('fourth.html')

    items = query_manager.query_4()
    data = []

    for item in items:
        el = {
            'id': item[0],
            'date': item[1].strftime("%d/%m/%y"),
        }
        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/5/', methods=['GET', 'POST'])
def fifth():
    if request.method == 'GET':
        return render_template('fifth.html')

    item = query_manager.query_5()
    data = []

    el = {
        'average_dist': str(item[0]),
        'average_time': str(item[1]),
    }
    data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/6/', methods=['GET', 'POST'])
def sixth():
    if request.method == 'GET':
        return render_template('fifth.html')

    items = query_manager.query_6()
    data = []

    for item in items:
        el = {
            'id': str(item[0]),
            'starting_address_1': str(item[1]),
            'starting_address_2': str(item[2]),
            'ending_address_1': str(item[3]),
            'ending_address_2': str(item[4]),
        }

        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/7/', methods=['GET', 'POST'])
def seventh():
    if request.method == 'GET':
        return render_template('fifth.html')
    items = query_manager.query_7()
    data = []

    for item in items:
        el = {
            'car_id': str(item[0])
        }

        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/8/', methods=['GET', 'POST'])
def eight():
    if request.method == 'GET':
        return render_template('fifth.html')
    items = query_manager.query_8()
    data = []

    for item in items:
        el = {
            'car_id': str(item[0])
        }

        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/9/', methods=['GET', 'POST'])
def nineth():
    if request.method == 'GET':
        return render_template('fifth.html')

    items = query_manager.query_9()
    data = []

    for item in items:
        el = {
            'workshop_id': str(item[0]),
            'name': str(item[1])
        }

        data.append(el)

    return render_template(
        'result.html',
        data=data
    )


@app.route('/10/', methods=['GET', 'POST'])
def tenth():
    if request.method == 'GET':
        return render_template('fifth.html')

    item = query_manager.query_10()[0]
    data = []

    el = {
        'car_id': str(item[0]),
        'price': str(item[1])
    }

    data.append(el)

    return render_template(
        'result.html',
        data=data
    )


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
