import config
from db_adapter import DBAdapter
from my_faker import fake_db


class QueryManager:
    db = DBAdapter(config.DB_DSN)

    def __init__(self):
        self.db.connect()
        self.db.init()
        # fake_db(self.db)

    def query_1(self, started_at='2018-01-01',
                ended_at='2018-11-25',
                color='red',
                plate='AN%',
                customer_id='8'):
        sql = """
        Select *
        from car
        where id IN(
        SELECT DISTINCT car_id
           from "order" as o
           join car as c on c.id = o.car_id
                              and o.started_at > %s
                              and o.ended_at < %s
                              and c.color = %s
                              and c.plate LIKE %s
                              and o.customer_id = %s
        );
        """
        data = (started_at, ended_at, color, plate, customer_id)

        rows = self.db.run_sql(sql, data).fetchall()

        return rows

    def query_2(self, date='2018-01-02'):
        sql = """
        select
          date_trunc('hour', ended_at) as interv_start,
          date_trunc('hour', ended_at)  + interval '1 hours' as ended_at,
         sum(1)
          from charging_station_car where %s < ended_at and ended_at < (date %s + interval '24 hour')
            group by date_trunc('hour', ended_at)
        order by interv_start
        """
        data = (date, date)

        rows = self.db.run_sql(sql, data).fetchall()
        return rows

    def query_3(self, start_date='2018-01-13'):
        sql = """SELECT
          sum(1) FILTER (WHERE started_at::time > '7:00:00' and started_at::time < '10:00:00') AS morning,
          sum(1) FILTER (WHERE started_at::time > '12:00:00' and started_at::time < '14:00:00') AS afternoon,
          sum(1) FILTER (WHERE started_at::time > '17:00:00' and started_at::time < '19:00:00') AS evening
        FROM
          \"order\"
        WHERE started_at > %s and started_at < (date %s + interval '7 day')
        """
        data = (start_date, start_date)

        rows = self.db.run_sql(sql, data).fetchall()
        return rows

    def query_4(self):
        sql = """
        select *
        from payment t1
        where (select count(*) from payment t2 where t1.amount = t2.amount
                                                 and t1.customer_id = t2.customer_id and t1.date = t2.date ) > 1
        order by amount
        """

        rows = self.db.run_sql(sql).fetchall()
        return rows

    def query_5(self):
        # fifth

        sql = """SELECT AVG(ST_Distance(ST_MakePoint(car_initial_point [ 0 ], car_initial_point [ 1 ]),
                               ST_MakePoint(point [ 0 ], point [ 1 ]))) as avg_dist,
               AVG(ended_at - started_at) as avg_time
        FROM "order" a
               LEFT JOIN address on a.starting_address = address.id"""

        avg_dist = self.db.run_sql(sql).fetchall()[0][0]
        avg_time = self.db.run_sql(sql).fetchall()[0][1]

        return avg_dist, avg_time

    def query_6(self):
        sql = """
        SELECT table_1.o, a1.address_line_1, a1.address_line_2, a2.address_line_1, a2.address_line_2
from
--      top 3 starting addresses
     ((SELECT '1' as o, starting_address, count(*)
       from "order"
       WHERE started_at :: time > '7:00:00'
         and started_at :: time < '10:00:00'
       group by starting_address
       LIMIT 1)
      UNION
      (SELECT '2' as o, starting_address, count(*)
       from "order"
       WHERE started_at :: time > '12:00:00'
         and started_at :: time < '14:00:00'
       group by starting_address
       order by count(*) DESC
       LIMIT 1)
      UNION
      (SELECT '3' as o, starting_address, count(*)
       from "order"
       WHERE started_at :: time > '17:00:00'
         and started_at :: time < '19:00:00'
       group by starting_address
       order by count(*) DESC
       LIMIT 1)) as table_1
       JOIN --          top 3 ending_addresses
         ((SELECT '1' as o, ending_address, count(*)
           from "order"
           WHERE started_at :: time > '7:00:00'
             and started_at :: time < '10:00:00'
           group by ending_address
           LIMIT 1)
          UNION
          (SELECT '2' as o, ending_address, count(*)
           from "order"
           WHERE started_at :: time > '12:00:00'
             and started_at :: time < '14:00:00'
           group by ending_address
           order by count(*) DESC
           LIMIT 1)
          UNION
          (SELECT '3' as o, ending_address, count(*)
           from "order"
           WHERE started_at :: time > '17:00:00'
             and started_at :: time < '19:00:00'
           group by ending_address
           order by count(*) DESC
           LIMIT 1)) as table_2 on table_1.o = table_2.o
    left join address as a1 on starting_address = a1.id
    left join address as a2 on ending_address = a2.id
order by table_1.o;
        """

        averages = self.db.run_sql(sql).fetchall()
        return averages

    def query_7(self):
        sql = """SELECT "car".*, COUNT("order".car_id) AS orders_count
        FROM "car"
               JOIN "order" ON car.id = "order".car_id
        GROUP BY car.id
        ORDER BY orders_count
        LIMIT (SELECT COUNT(id) / 10.0 FROM car);"""
        top_10 = self.db.run_sql(sql).fetchall()
        return top_10

    def query_8(self, date='2018-11-10'):
        sql = """SELECT customer_id, count(*)
        from "order"
               LEFT JOIN car on car.id = "order".car_id
               LEFT JOIN charging_station_car on charging_station_car.car_id = car.id
        WHERE "order".started_at > %s
          and "order".ended_at < (date %s + interval '1 month')
          and ("charging_station_car".ended_at :: date) = "order".started_at :: date
        GROUP BY customer_id"""

        data = (date, date)

        pairs = self.db.run_sql(sql, data).fetchall()
        return pairs

    def query_9(self):
        sql = """
        SELECT workshop_id, name FROM (SELECT DISTINCT ON (workshop_car_id, weekly) workshop_car_id, car_part_id, COUNT(car_part_id) as types_count, "name", date_trunc('week', bought_at :: date) AS weekly
        FROM workshop_car_car_part
               JOIN car_part on car_part_id = car_part.id
        GROUP BY weekly, workshop_car_id, car_part_id, car_part.name
        ORDER BY weekly, workshop_car_id, types_count DESC) as t LEFT JOIN workshop_car on workshop_car.id=workshop_car_id"""

        most_popular_per_week = self.db.run_sql(sql).fetchall()

        return most_popular_per_week

    def query_10(self):
        sql = """
        Select a.id, (sum + cost_charging) price
        from (SELECT car.id, SUM(car_part.price)
              from car
                     left join workshop_car on workshop_car.car_id = car.id
                     left join workshop_car_car_part on workshop_car_car_part.id = workshop_car_car_part.workshop_car_id
                     left join car_part on car_part.id = workshop_car_car_part.car_part_id
              GROUP BY car.id) as a
               JOIN (SELECT car.id,
                            charging_station.price_per_hour * (extract(epoch from (charging_station_car.ended_at :: timestamp -
                                                                                   charging_station_car.started_at :: timestamp))) /
                            3600 as cost_charging
                     from car
                            left join charging_station_car on car.id = charging_station_car.car_id
                            left join charging_station on charging_station_car.charging_station_id = charging_station.id
                     order by cost_charging DESC) as b on a.id = b.id
        order by price DESC LIMIT 1;"""

        the_worst_car = self.db.run_sql(sql).fetchall()

        return the_worst_car
