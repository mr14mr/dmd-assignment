import psycopg2


class DBAdapter:
    DB_DSN = None
    connection = None

    def __init__(self, db_dsn):
        self.DB_DSN = db_dsn

    def connect(self):
        try:
            self.connection = psycopg2.connect(self.DB_DSN)
        except:
            print("Unable to connect to the database")

    def run_sql(self, sql, data=None):
        cur = self.connection.cursor()
        try:
            cur.execute(sql, data)
        except psycopg2.IntegrityError as e:
            print(e)

        self.connection.commit()

        return cur

    def init(self):
        text = open('init.sql', 'r').read()

        cur = self.connection.cursor()
        cur.execute(text)
        self.connection.commit()
