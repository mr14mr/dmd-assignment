import os

DB_NAME = os.environ.get('DB_NAME', 'dmd_high_grade_pls')
DB_HOSTNAME = os.environ.get('DB_HOSTNAME', 'db')
DB_HOSTPORT = os.environ.get('DB_HOSTPORT', '5432')
DB_USERNAME = os.environ.get('DB_USERNAME', 'dmd_high_grade_pls')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'dmd_high_grade_pls')
DB_DSN = f"user='{DB_USERNAME}' password='{DB_PASSWORD}' dbname='{DB_NAME}' host='{DB_HOSTNAME}' port={DB_HOSTPORT}"
