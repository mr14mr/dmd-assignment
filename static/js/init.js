(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.modal').modal();
    $('.datepicker').datepicker();

  }); // end of document ready
})(jQuery); // end of jQuery name space

$(document).ready(function(){
    $('.timepicker').timepicker();
  });