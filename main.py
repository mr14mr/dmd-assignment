import config
from db_adapter import DBAdapter
from my_faker import fake_db
from queries import QueryManager

db = DBAdapter(config.DB_DSN)
db.connect()

db.init()

# let's fake some data
# fake_db(db)

# first
#
# started_at = '2018-01-01'
# ended_at = '2018-11-25'
# color = 'red'
# plate = 'AN%'
# customer_id = '8'
#
# sql = """
# Select *
# from car
# where id IN(
# SELECT DISTINCT car_id
#    from "order" as o
#    join car as c on c.id = o.car_id
#                       and o.started_at > %s
#                       and o.ended_at < %s
#                       and c.color = %s
#                       and c.plate LIKE %s
#                       and o.customer_id = %s
# );
# """
# data = (started_at, ended_at, color, plate, customer_id)
#
# rows = db.run_sql(sql, data).fetchall()
# for r in rows:
#     print(r)
#
# # second
# date = '2018-01-02'
#
# sql = """
# select
#   date_trunc('hour', ended_at) as interv_start,
#   date_trunc('hour', ended_at)  + interval '1 hours' as ended_at,
#  sum(1)
#   from charging_station_car where %s < ended_at and ended_at < (date %s + interval '24 hour')
#     group by date_trunc('hour', ended_at)
# order by interv_start
# """
# data = (date, date)
#
# rows = db.run_sql(sql, data).fetchall()
# for r in rows:
#     print(r)
#
# # third
# start_date = '2018-11-10'
# sql = """SELECT
#   sum(1) FILTER (WHERE started_at::time > '7:00:00' and started_at::time < '10:00:00') AS morning,
#   sum(1) FILTER (WHERE started_at::time > '12:00:00' and started_at::time < '14:00:00') AS afternoon,
#   sum(1) FILTER (WHERE started_at::time > '17:00:00' and started_at::time < '19:00:00') AS evening
# FROM
#   \"order\"
# WHERE started_at > %s and started_at < (date %s + interval '7 day')
# """
# data = (start_date, start_date)
#
# rows = db.run_sql(sql, data).fetchall()
# for r in rows:
#     print(r)
#
# # fourth
# precision = '0 sec'
# sql = """
# select *
# from payment t1
# where (select count(*) from payment t2 where t1.amount = t2.amount
#                                          and t1.customer_id = t2.customer_id and (t1.date - t2.date) > interval %s ) > 1
# order by amount
# """
#
# data = (precision,)
#
# rows = db.run_sql(sql, data).fetchall()
# print(f"Douplicates: {len(rows)}")
#
# # fifth
#
# sql = """SELECT AVG(ST_Distance(ST_MakePoint(car_initial_point [ 0 ], car_initial_point [ 1 ]),
#                        ST_MakePoint(point [ 0 ], point [ 1 ]))) as avg_dist,
#        AVG(ended_at - started_at) as avg_time
# FROM "order" a
#        LEFT JOIN address on a.starting_address = address.id"""
#
# avg_dist = db.run_sql(sql).fetchall()[0][0]
# avg_time = db.run_sql(sql).fetchall()[0][1]
# print(f"Average distance: {avg_dist}")
# print(f"Average time: {avg_time}")
#
# # sixth
#
# sql = """
# (SELECT '1' as o, starting_address, ending_address, count(*)
#  from "order"
#  WHERE started_at :: time > '7:00:00'
#    and started_at :: time < '10:00:00'
#  group by starting_address, ending_address
#  LIMIT 1) UNION (SELECT '2' as o, starting_address, ending_address, count(*)
#                  from "order"
#                  WHERE started_at :: time > '12:00:00'
#                    and started_at :: time < '14:00:00'
#                  group by starting_address, ending_address
#                  order by count(*) DESC
#                  LIMIT 1)
#           UNION (SELECT '3' as o, starting_address, ending_address, count(*)
#                  from "order"
#                  WHERE started_at :: time > '17:00:00'
#                    and started_at :: time < '19:00:00'
#                  group by starting_address, ending_address
#                  order by count(*) DESC
#                  LIMIT 1) order by o;
# """
#
# averages = db.run_sql(sql).fetchall()
# print(averages)
#
# # seventh
#
# sql = """SELECT "car".*, COUNT("order".car_id) AS orders_count
# FROM "car"
#        JOIN "order" ON car.id = "order".car_id
# GROUP BY car.id
# ORDER BY orders_count
# LIMIT (SELECT COUNT(id) / 10.0 FROM car);"""
# top_10 = db.run_sql(sql).fetchall()
# print(top_10)
#
# # eighth
# date = '2018-11-10'
# sql = """SELECT customer_id, count(*)
# from "order"
#        LEFT JOIN car on car.id = "order".car_id
#        LEFT JOIN charging_station_car on charging_station_car.car_id = car.id
# WHERE "order".started_at > %s
#   and "order".ended_at < (date %s + interval '1 month')
#   and ("charging_station_car".ended_at :: date) = "order".started_at :: date
# GROUP BY customer_id"""
#
# data = (date, date)
#
# pairs = db.run_sql(sql, data).fetchall()
# print(pairs)
#
# # nineth
#
# sql = """
# SELECT DISTINCT ON (workshop_car_id, weekly) workshop_car_id, car_part_id, COUNT(car_part_id) as types_count, "name", date_trunc('week', bought_at :: date) AS weekly
# FROM workshop_car_car_part
#        JOIN car_part on car_part_id = car_part.id
# GROUP BY weekly, workshop_car_id, car_part_id, car_part.name
# ORDER BY weekly, workshop_car_id, types_count DESC"""
#
# most_popular_per_week = db.run_sql(sql).fetchall()
#
# print(most_popular_per_week)
#
# # tenth
#
# sql = """
# Select a.id, (sum + cost_charging) price
# from (SELECT car.id, SUM(car_part.price)
#       from car
#              left join workshop_car on workshop_car.car_id = car.id
#              left join workshop_car_car_part on workshop_car_car_part.id = workshop_car_car_part.workshop_car_id
#              left join car_part on car_part.id = workshop_car_car_part.car_part_id
#       GROUP BY car.id) as a
#        JOIN (SELECT car.id,
#                     charging_station.price_per_hour * (extract(epoch from (charging_station_car.ended_at :: timestamp -
#                                                                            charging_station_car.started_at :: timestamp))) /
#                     3600 as cost_charging
#              from car
#                     left join charging_station_car on car.id = charging_station_car.car_id
#                     left join charging_station on charging_station_car.charging_station_id = charging_station.id
#              order by cost_charging DESC) as b on a.id = b.id
# order by price DESC LIMIT 1;"""
#
# the_worst_car = db.run_sql(sql).fetchall()
#
# print(the_worst_car)

query_manager = QueryManager()

print(query_manager.query_10())
# print(query_manager.query_7())
